package com.example.firebaseremoteconfigapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;


public class MainActivity extends AppCompatActivity {

    public static final String TAG= "MainActivity";

    FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    TextView textView;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String DEFAULT_TEXT_KEY="text";
    public static final String DEFAULT_TEXT_VALUE="Hello World!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences=getPreferences(MODE_PRIVATE);
        editor=sharedPreferences.edit();
        setupTextView();

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(10)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);


        mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if(task.isSuccessful())
                {
                    //Toast.makeText(MainActivity.this, "Fetched Successfully",Toast.LENGTH_SHORT).show();

                    //if fetching is successful update the shared preferences value
                    editor.putString(DEFAULT_TEXT_KEY,mFirebaseRemoteConfig.getString(DEFAULT_TEXT_KEY));
                    editor.apply();
                    Log.d(TAG, "onComplete: value updated to "+sharedPreferences.getString(DEFAULT_TEXT_KEY,DEFAULT_TEXT_VALUE));
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Fetching Failed",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setupTextView() {
        textView = findViewById(R.id.textView);
        if(sharedPreferences.contains(DEFAULT_TEXT_KEY))
            textView.setText(sharedPreferences.getString(DEFAULT_TEXT_KEY,DEFAULT_TEXT_VALUE));
    }
}
